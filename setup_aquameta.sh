# docker
apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
echo deb https://apt.dockerproject.org/repo ubuntu-trusty main > /etc/apt/sources.list.d/docker.list
apt-get update
apt-get purge lxc-docker
apt-get install -y docker-engine git vim tmux
usermod -aG docker ubuntu

# aquameta
cd ~ubuntu
mkdir -p src && cd src && git clone https://github.com/aquametalabs/aquameta.git
cd aquameta
docker build -t aquameta/aquameta:0.08 .

# start + scripts
cd ~ubuntu/
echo docker run -dit -p 80:80 -p 8080:8080 -p 5432:5432 --privileged aquameta/aquameta:0.08 > start.sh
chmod ugo+x start.sh
./start.sh 

echo docker exec -it `docker ps -q | head -1` psql -U postgres aquameta > dbshell.sh
chmod +x dbshell.sh

echo docker exec -it `docker ps -q | head -1` bash > shell.sh
chmod +x shell.sh

